package a4_4;

import java.util.Scanner;

public class Schleifen2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Aufgabe 1: Z�hlen
		// Geben Sie in der Konsole die nat�rlichen Zahlen von 1 bis n heraufz�hlend
		// (bzw. von n bis 1
		// herunterz�hlend) aus. Erm�glichen Sie es dem Benutzer die Zahl n festzulegen.
		// Nutzen Sie
		// zur Umsetzung eine while-Schleife.

		// a) 1, 2, 3, �, n while-Schleife
		System.out.println("Aufgabe 1a: Z�hlen *");

		Scanner myScannerA = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine nat�rliche Zahl ein: ");
		int numA = myScannerA.nextInt();
		int count = 1;

		while (count <= numA) {
			if (count == numA) {
				System.out.print(count);
			} else {
				System.out.print(count + ", ");
			}
			count++;
		}

		// b) n, �, 3, 2, 1 while-Schleife
		System.out.println("\n\nAufgabe 1b: Z�hlen *");

		Scanner myScannerB = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine nat�rliche Zahl ein: ");
		int numB = myScannerB.nextInt();

		while (numB >= 1) {
			if (numB == 1) {
				System.out.println(numB);
			} else {
				System.out.print(numB + ", ");
			}
			numB--;
		}

		// Aufgabe 2: Fakult�t
		// Schreiben Sie ein Programm, das zu einer Zahl n <= 20 die Fakult�t n!
		// ermittelt.
		// Es gilt:
		// n! = 1 * 2 * ... * (n-1) * n sowie 0! = 1
		// z.B. ist 3! = 1 * 2 * 3 = 6

		System.out.println("\n\nAufgabe 2: Fakult�t **");

		Scanner myScanner2 = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine nat�rliche Zahl bis 20 ein: ");
		int n = myScanner2.nextInt();
		long fakultaet = 1;
		int faktor = 1;

		while (faktor <= n) {
			fakultaet = fakultaet * faktor;
			faktor++;
		}
		System.out.println("n! ist: " + fakultaet);

		// Aufgabe 3: Quersumme
		// Berechnen Sie die Quersumme einer vorgegebenen ganzen Zahl. Die Quersumme
		// einer
		// Zahl ist die Summe aller ihrer Ziffern.
		// Tipp: Nutzen Sie den Modulo Operator.
		// z.B.: Geben sie bitte eine Zahl ein: 12345
		// Die Quersumme betr�gt: 15

		System.out.println("\n\nAufgabe 3: Quersumme **");

		Scanner querScanner = new Scanner(System.in);
		System.out.print("Geben Sie bitte eine (zweistellige ganze) Zahl: ");
		int zahl = querScanner.nextInt(); // 123456789
		int quersumme = 0;
		int rest;

		while (zahl > 0) {
			rest = zahl % 10;
			quersumme = quersumme + rest;
			zahl = zahl / 10;
		}
		System.out.println("Die Quersumme betr�gt: " + quersumme);

		//Aufgabe 4: Temperaturumrechnung **
		//Erstellen und testen Sie ein Java-Programm, das dem dargestellten Beispiel entsprechend 
		//Temperaturwerte von Celsius nach Fahrenheit umrechnet.
		
		//Bitte den Startwert in Celsius eingeben: -10
		//Bitte den Endwert in Celsius eingeben: 20
		//Bitte die Schrittweite in Grad Celsius eingeben: 5
		//-10,00�C	14,00�F
		//-5,00�C	23,00�F
		//0,00�C	32,00�F
		//5,00�C	41,00�F
		//10,00�C	50,00�F
		//15,00�C	59,00�F
		//20,00�C	68,00�F
		
		Scanner temperaturScanner = new Scanner(System.in);
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		int celsiusStart = temperaturScanner.nextInt();
		System.out.println("Bitte den Endwert in celsius eingeben: ");
		int celsiusEnd = temperaturScanner.nextInt();
		System.out.println("Bitte die Schrittweite in Grad Celsius eingeben: ");
		int celsiusSchritt = temperaturScanner.nextInt();
		
	}
}
