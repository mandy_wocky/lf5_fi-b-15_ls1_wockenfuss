package a4_4;
import java.util.Scanner;

public class Schleifen1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Aufgabe 1: Z�hlen
		// Geben Sie in der Konsole die nat�rlichen Zahlen von 1 bis n heraufz�hlend
		// (bzw. von n bis 1
		// herunterz�hlend) aus. Erm�glichen Sie es dem Benutzer die Zahl n festzulegen.
		// Nutzen Sie
		// zur Umsetzung eine for-Schleife.

		System.out.println("Aufgabe 1: Z�hlen *");
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine nat�rlich Zahl ein:");
		int n = myScanner.nextInt();

		for (int i = n; i >= 0; i--) {
			System.out.print(i + ", ");
		}
		
		// - - - - - - - - - - - - - - - - - - - -

		// Aufgabe 2: Summe
		// Geben Sie in der Konsole die Summe der Zahlenfolgen aus. Erm�glichen Sie es
		// dem
		// Benutzer die Zahl x festzulegen, welche die Summierung begrenzt.
		// a) 1 + 2 + 3 + 4 +�+ x for-Schleife / while-Schleife
		// b) 2 + 4 + 6 +�+ 2x for-Schleife / while-Schleife
		// c) 1 + 3 + 5 +�+ (2x+1) for-Schleife / while-Schleife
		
		// zu a:
		System.out.println("\n\nAufgabe 2: Summe *");
		
		int zahlA = 1;
		int zahlB = 2;
		int zahlC = 1;
		int summeA = 0;
		int summeB = 0;
		int summeC = 0; 
		
		Scanner sumAScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte einen begrenzenden Wert ein:");
		int x = sumAScanner.nextInt();
		
		do {
			summeA = summeA + zahlA;
			zahlA++;
		} while(zahlA <= x);
		
		System.out.println("Die Summe f�r A betr�gt:" + summeA);
		
		// zu b:
		
		do {
			summeB = summeB + zahlB;
			zahlB = zahlB + 2;
		} while(zahlB <= x * 2);
		
		System.out.println("Die Summe f�r B betr�gt:" + summeB);
	
		// zu c:
		do {
		summeC = summeC + zahlC;
		zahlC = zahlC + 2;
		} while(zahlC <= (2 * x) + 1);
	
		System.out.println("Die Summe f�r C betr�gt:" + summeC);
		
		// - - - - - - - - - - - - - - - - - - - -
		
		//Aufgabe 3: Modulo
		//Schreibe ein Programm, das f�r alle Zahlen zwischen 1 und 200 testet:
		//> ob sie durch 7 teilbar sind.
		//> nicht durch 5 aber durch 4 teilbar sind.
		//Lasse jeweils die Zahlen, auf die die Bedingungen zutreffen ausgeben.

		System.out.println("\nAufgabe 3: Modulo *");
		int z = 1;
				
		while(z >= 1 && z <= 200) {
			z++;
			if (z % 7 == 0 && z % 4 == 0 && !(z % 5 == 0)) {
				System.out.print(z + ", ");
			}
		}
		
		// - - - - - - - - - - - - - - - - - - - -
		
		//Aufgabe 4: Folgen
		//Programmieren Sie Schleifen, welche die folgenden Folgen ausgeben:
		//a) 99, 96, 93, � 12, 9
		//b) 1, 4, 9, 16, 25, � 361, 400
		//c) 2, 6, 10, 14, � 98, 102
		//d) 4, 16, 36, 64, 100 � 900, 1024
		//e) 2, 4, 8, 16, 32, �, 16384, 32768
		
		// a:
		System.out.println("\n\nAufgabe 4a: Folgen **");
		for(int i = 99; i >= 9; i-=3) {
			System.out.print(i + ", ");
		}
		
		// b:
		System.out.println("\n\nAufgabe 4b: ");
		for(int j = 1; j <= 20; j++){
			System.out.print((j*j)+ ", ");
		}
		
		// c:
		System.out.println("\n\nAufgabe 4c: ");
		for(int k = 2; k <= 32; k+=2){
			System.out.print((k*k)+ ", ");
		}
		
		// d:
		System.out.println("\n\nAufgabe 4d: ");
		for(int l = 2; l <= 16384; l*=2){
			System.out.print((l) + ", ");
		}
		
		//Aufgabe 5: Einmaleins
		//Sie sollen ein Programm entwickeln, welches das kleine
		//Einmaleins (1x1, 1x2 � bis 10x10) auf dem Bildschirm
		//ausgibt. 
		
		System.out.println("\n\nAufgabe 5: Einmaleins **");
		for(int faktor1=1; faktor1<=10; faktor1++) {
            for(int faktor2=1; faktor2<=10; faktor2++) {
                if(faktor2<10) {
                    System.out.printf("%4s", faktor1*faktor2);
                } else {
                    System.out.println(faktor1*faktor2);
                }
            }
        }
		
		//Aufgabe 6: Sterne
		//Schreiben Sie ein Programm, das eine von Ihnen vorgegebene Anzahl von Sternen (*) in
		//Form eines Dreiecks auf dem Bildschirm ausgibt.
		//Tipp: Es existiert eine L�sung unter Nutzung einer Schleife und eine weitere L�sung mit zwei
		//verschachtelten Schleifen.
		
		System.out.println("\nAufgabe 6: Sterne **");
		
		int zeilen=5;
		
		for(int m=0; m<zeilen; m++) {
			for(int p=0; p<m+1; p++) {
				System.out.print("*");		
				}
			    System.out.println("");
			}

		// Aufgabe 7: Primzahlen
		// Entwickeln Sie ein Programm, welches die ersten hundert Primzahlen auf dem
		// Bildschirm ausgibt.
		// P = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67,
		// 71, 73, 79, 83, 89,
		// 97)

		System.out.println("\nAufgabe 7: Primzahlen **");

		int counter = 0;

		for (int divident = 2; divident <= 100; divident++) {
			Boolean primzahlJa = true;

			for (int divisor = 2; divisor < divident; divisor++) {
				if (divident % divisor == 0) {
					primzahlJa = false;
					break;
				}
			}

			if (primzahlJa == true) {
				counter++;
				System.out.print(divident + ", ");
			}
		}
	}
}
