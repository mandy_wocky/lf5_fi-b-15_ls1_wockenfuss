public class Methoden2 {

	public static void main(String[] args) {
		// Aufgabe 2 - Multiplikation
		
		double x = 2.36;
		double y = 7.87;
		
		//Methodenaufruf folgt:
		System.out.println(multiplikation(x, y));
	}

	public static double multiplikation(double faktor1, double faktor2) {
	return faktor1 * faktor2;
}
}
