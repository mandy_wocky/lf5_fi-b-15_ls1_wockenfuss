
public class Methoden3 {

	public static void main(String[] args) {
		// Aufgabe3 - Volumen
		
		double seiteA = 4;
		double seiteB = 4.5;
		double seiteC = 5.5;
		double h�heH = 10;
		double radius = 5;
		final double pi = 3.1415926;
		
		System.out.println(wuerfel(seiteA) + " cm�");
		System.out.println(quader(seiteA, seiteB, seiteC) + " cm�");
		System.out.println(pyramide(seiteA, h�heH) + " cm�");
		System.out.println(kugel(radius, pi) + " cm�");
	}
	
//Definition der Methoden
	public static double wuerfel(double a) {
		return a * a * a;
	}
	
	public static double quader(double a, double b, double c) {
		return a * b * c;
	}
		
	public static double pyramide(double a, double h) {
		return a * a * h / 3;
	}
	
	public static double kugel(double r, double pi) {
		return 4/3 * (r * r * r) * pi;
	}
}
