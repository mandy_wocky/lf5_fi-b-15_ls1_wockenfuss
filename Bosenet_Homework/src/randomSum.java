import java.util.Random;

public class randomSum {

	public static void main(String[] args) {
		// Please generate 10 random numbers between 25 and 50 and print their sum.
		// (No user inputs)

		Random random = new Random();
		int result = 0;
		
		for(int i = 1; i <= 10; i++) {
			int num = random.nextInt(50 - 25) + 25;
			result = result + num;
			
			if(i < 10) {
			System.out.print(num + " + ");
			}
			
			if(i == 10) {
				System.out.print(num);
			}
		}
		
		System.out.print(" = " + result);
	}

}
