import java.util.Random;
import java.util.Scanner;

public class bulls_eye {

	public static void main(String[] args) {
		/*
		 * Q2. Write a program to input two numbers (num1 and num2), and let computer
		 * think a random number between them. It will keep on asking user to guess the
		 * number computer have thought!If User enters a number above the number
		 * computer thought it will print "enter lower number" and if user inputs less
		 * number than computers mind then computer says "enter higher number". And if
		 * it matches computer says "Bulls Eye". Program should keep on asking until
		 * Bulls eye is reached. Program should count number of guesses and print it too
		 * - like "Number of guesses made ="xx�
		 * 
		 * example: 
		 * Enter number 1: 1 
		 * Enter number 2: 200 
		 * Guess the number computer have thought: 100 
		 * Enter lower number: 50 
		 * Enter higher number: 70 
		 * Enter higher number: 75 
		 * Enter higher number: 85 
		 * Enter lower number: 80 
		 * Enter higher number: 82 
		 * Too close! enter lower number: 81
		 * 
		 * Bulls Eye
		 * 
		 * Number of guesses made = 8
		 */
		
		System.out.println("Hi non-computer, let's play a game!");
		System.out.println("***********************************");
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int num1 = scanner.nextInt();
		System.out.print("Enter another number: ");
		int num2 = scanner.nextInt();
		System.out.println("\nOne last thing:\n---------------");
		System.out.println("If you need more than 6 guesses,\nthere won't be any homework today!\n");
		System.out.println("But if you need just up to 6 guesses,\nyou immediately can quit work for today ^_^\n");
		System.out.println("***********************************");
		
		System.out.print("Guess a number between " + num1 + " and " + num2 + ": ");

		Random generator = new Random();
		int random = generator.nextInt(num2 - num1 + 1) + num1;
		int guess;
		int counter = 0;

		do {
			guess = scanner.nextInt();

			if (guess == random) {
				System.out.println("\n**********************");
				System.out.println("!!!!!BULLS EYE!!!!!");
				System.out.println("**********************");
			}
			if (guess == random + 1) {
				System.out.print("\nOMG, I'm getting a heart attack! Too close!\nEnter lower number: ");
			}
			if (guess == random - 1) {
				System.out.print("\nOMG, are you kidding me?! Too close!\nEnter higher number: ");
			}			
			if (guess >= random - 10 && guess < random - 1) {
				System.out.print("\nYou are good... but not good enough!\nEnter higher number: ");
			}
			if (guess <= random + 10 && guess > random + 1) {
				System.out.print("\nYou are good... but not good enough!\nEnter lower number: ");
			}
			if (guess > random + 10) {
				System.out.print("\nYou couldn't be further away!\nEnter lower number: ");
			}
			if (guess < random - 10) {
				System.out.print("\nYou couldn't be further away!\nEnter higher number: ");
			}
			
			counter++;
			
		} while (guess != random);
		
		System.out.println("Number of guesses: " + counter);
	}
}
