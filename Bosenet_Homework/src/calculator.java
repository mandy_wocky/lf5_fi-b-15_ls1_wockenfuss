import java.util.Scanner;

public class calculator {

	public static void main(String[] args) {
		/*
		 * Q1. Write a program to display menu like:
		 * 
		 * Maths Menu options 
		 * ------------------- 
		 * 1. Add two numbers 
		 * 2. Substract twp numbers
		 * 3. Multiply two numbers 
		 * 4. Divide first number by second number 
		 * 5. Exit
		 * 
		 * Enter your choice: 2 
		 * ----- 
		 * Enter number 1: 10 
		 * Enter number 2: 5
		 * Multiplication result is 50
		 * 
		 * Maths Menu options 
		 * -------------------
		 * 1. Add two numbers 
		 * 2. Subtract two numbers
		 * 3. Multiply two numbers 
		 * 4. Divide first number by second number 
		 * 5. Exit
		 * 
		 * Enter your choice: 5
		 * 
		 * Thank you
		 */

		int option, ex;

		do {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Maths Menu options");
			System.out.println("------------------");
			System.out.println("1. Add two numbers");
			System.out.println("2. Subtract two numbers");
			System.out.println("3. Multiply two numbers");
			System.out.println("4. Divide first number by second number");
			System.out.println("5. Exit");
//			System.out.println();
			System.out.println("\nEnter your choice:");		//added \n for blank line

			option = scanner.nextInt();
			if (option == 5) {
				break;
			}

			switch (option) {
			case 1:
				addition(option);
				break;
			case 2:
				subtraction(option);
				break;
			case 3:
				multiplication(option);
				break;
			case 4:
				division(option);
				break;
			case 5:
				break;
			default:
				System.out.println("Invalid choice");
			}

			System.out.println("Do you want to continue? 1. Yes 2. No");
			ex = scanner.nextInt();
		} while (ex == 1);

		System.out.println("Thank you");
	}

	// Methods
	public static void addition(int option) {

		if (option != 5) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Enter the first number");
			int num1 = scanner.nextInt();
			System.out.println("Enter the second number");
			int num2 = scanner.nextInt();
			System.out.println("Addition of " + num1 + " and " + num2 + "is" + (num1 + num2));
		}
	}

	public static void subtraction(int option) {

		if (option != 5) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Enter the first number");
			int num1 = scanner.nextInt();
			System.out.println("Enter the second number");
			int num2 = scanner.nextInt();
			System.out.println("Subtraction of " + num1 + " and " + num2 + " is " + (num1 - num2));
		}
	}

	public static void multiplication(int option) {

		if (option != 5) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Enter the first number");
			int num1 = scanner.nextInt();
			System.out.println("Enter the second number");
			int num2 = scanner.nextInt();
			System.out.println("Multiplication of " + num1 + " and " + num2 + " is    " + (num1 * num2));
		}
	}

	public static void division(int option) {

		if (option != 5) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Enter the first number");
			int num1 = scanner.nextInt();
			System.out.println("Enter the second number");
			int num2 = scanner.nextInt();
			if (num2 == 0) {
				System.out.println("Error!!! In Division denominator cannot be 0!");
			} else {
				System.out.println("In division of " + num1 + " by " + num2 + " quotient is " + (num1 / num2)
						+ " and remainder is " + (num1 % num2));
			}
		}
	}
}