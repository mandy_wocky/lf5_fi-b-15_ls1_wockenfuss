import java.util.Scanner;

public class randomFactors {

	public static void main(String[] args) {
		// Input any number (between 10 and 1000) and print all its factors.
		// Example:
		// Input a number: 20
		// The factors of 20 are 1, 2, 4, 5, 10 and 20

		Scanner scanner = new Scanner(System.in);
		System.out.print("Input a number between 10 and 1000: ");
		int num = scanner.nextInt();
		int i;

		System.out.print("The factors of " + num + " are ");
		
		for (i = 1; i <= num; i++) {

			if(i < num/2 && num % i == 0) {
				System.out.print(i + ", ");
			}
			
			if(i == num/2) {
				System.out.print(i + " ");
			}
			
			if(i == num) {
				System.out.println("and " + i);
			}
		}
	}
}
