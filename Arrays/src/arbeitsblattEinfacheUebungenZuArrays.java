import java.util.Scanner;

public class arbeitsblattEinfacheUebungenZuArrays {

	public static void main(String[] args) {

//		aufgabe1();
//		aufgabe2();
//		aufgabe3();
//		aufgabe3Spezifischer();
		aufgabe4();
	}

	/* Aufgabe 1 
	 * Schreiben Sie ein Programm �Zahlen�, in welchem ein ganzzahliges
	 * Array der L�nge 10 deklariert wird. Anschlie�end wird das Array mittels
	 * Schleife mit den Zahlen von 0 bis 9 gef�llt. Zum Schluss geben Sie die
	 * Elemente des Arrays wiederum mit einer Schleife auf der Konsole aus.
	 */
	public static void aufgabe1() {
		
		//Deklaration
		int[] zeichenkette = new int[10];

		//Array f�llen
		for (int i = 0; i < zeichenkette.length; i++) {
			zeichenkette[i] = i;
		}

		//Array ausgeben
		for (int i = 0; i < zeichenkette.length; i++) {
			System.out.println(zeichenkette[i]);

		}
	}

	/* Aufgabe 2 
	 * Das zu schreibende Programm �UngeradeZahlen� ist �hnlich der
	 * Aufgabe 1. Sie deklarieren wiederum ein Array mit 10 Ganzzahlen. Danach
	 * f�llen Sie es mit den ungeraden Zahlen von 1 bis 19 und geben den Inhalt des
	 * Arrays �ber die Konsole aus (Verwenden Sie Schleifen!).
	 */
	public static void aufgabe2() {
		
		//Deklaration
		int[] zeichenkette = new int[10];
		int x = 1;

		// Array f�llen
		for (int i = 0; i < zeichenkette.length; i++) {

			zeichenkette[i] = x;
			x += 2;
		}

		//Array ausgeben
		for (int i = 0; i < zeichenkette.length; i++) {
			System.out.println(zeichenkette[i]);
		}
	}

	/* Aufgabe 3 
	 * Im Programm �Palindrom� werden �ber die Tastatur 5 Zeichen
	 * eingelesen und in einem geeigneten Array gespeichert. Ist dies geschehen,
	 * wird der Arrayinhalt in umgekehrter Reihenfolge (also von hinten nach vorn)
	 * auf der Konsole ausgegeben.
	 */
	
	public static void aufgabe3() {
		
		//Deklaration
		char[] zeichenkette = new char[5];
		System.out.println("Gib f�nf Zahlen ein: ");
		
		//Array f�llen
		for(int i = 0; i < zeichenkette.length; i++) {
			Scanner scanner = new Scanner(System.in);
			char palindrom = scanner.next().charAt(0);
			zeichenkette[i] = palindrom;
		}
		
		//Array ausgeben
		for(int i = 4; i >= 0; i--) {
			System.out.println(zeichenkette[i]);
		}
	}
	public static void aufgabe3Spezifischer() {
		
		//Deklaration
		String palindrom = "";
		char[] zeichenkette = new char[5];

		System.out.println("Geben Sie f�nf Zeichen: ");
		// Array f�llen
		Scanner scanner = new Scanner(System.in);
		palindrom = scanner.next();
		for (int i = 0; i < zeichenkette.length; i++) {

			zeichenkette[i] = palindrom.charAt(i);
		}

		//Array ausgeben
		for (int i = 4; i >= 0; i--) {
			System.out.print(zeichenkette[i]);
		}
	}
	
	/* Aufgabe 4 
	 * Jetzt wird Lotto gespielt. In der Klasse �Lotto� gibt es ein
	 * ganzzahliges Array, welches 6 Lottozahlen von 1 bis 49 aufnehmen kann.
	 * Konkret sind das die Zahlen 3, 7, 12, 18, 37 und 42. Tragen Sie diese im
	 * Quellcode fest ein. 
	 * 
	 * a) Geben Sie zun�chst schleifenbasiert den Inhalt des
	 * Arrays in folgender Form aus: [ 3 7 12 18 37 42 ] 
	 * 
	 * b) Pr�fen Sie nun nacheinander, ob die Zahlen 12 bzw. 13 in der Lottoziehung 
	 * vorkommen. Geben Sie nach der Pr�fung aus: 
	 * 		
	 * "Die Zahl 12 ist in der Ziehung 
	 * enthalten. Die Zahl 13 ist nicht in der Ziehung enthalten."
	 */

	public static void aufgabe4() {
		//Deklaration & Array f�llen
		 int[] lottery = { 3, 7, 12, 18, 37, 42 }; 
		 
		//Array ausgeben
		for (int i = 0; i < lottery.length; i++) {
			if(i == 0) {
			System.out.print("[ " + lottery[0] + " ");
			} else if (i != 0 && i != 5) {
			System.out.print(lottery[i] + " ");
			} else if (i == 5) {
			System.out.print(lottery[5] + " ]");
			}
		}
	}
}
