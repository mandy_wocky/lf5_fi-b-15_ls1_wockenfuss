﻿import java.util.Scanner;

class Fahrkartenautomat {
	
	//globale Variable:
	static Scanner tastatur = new Scanner(System.in);

	//Hauptmethode:
	public static void main(String[] args) {

		//Ausgabe der Methoden:
		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double rückgabe = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rückgabe); 		
	}

	//Methode fahrkartenbestellungErfassen (Ergebnis Typ double; keine Paramter):
	public static double fahrkartenbestellungErfassen(){
		
		System.out.print("Zu zahlender Betrag (EURO): ");
		double ticketpreis = tastatur.nextDouble();
	
		System.out.print("Wie viele Tickets möchten Sie erwerben?: ");
		int ticketAnzahl = tastatur.nextInt();
		
		if(ticketAnzahl < 1 || ticketAnzahl > 10) {
			System.out.println("Fehler: Die Ticketanzahl wurde auf 1 geändert.");
			ticketAnzahl = 1;
		}
		
		if(ticketpreis < 0) {
			System.out.println("Fehler: Der Ticketpreis wird auf 1 gesetzt.");
			ticketpreis = 1;
		}
		
		double zuZahlenderBetrag = ticketAnzahl * ticketpreis;
		return zuZahlenderBetrag;

	}
	
	//Methode fahrkartenBezahlen (zuZahlenderBetrag als als formaler Parameter;
	//Rückgabebetrag als Ergebnis):
	public static double fahrkartenBezahlen(double zuZahlenderBetrag ){
		
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze = 0.0;
		
		while (eingezahlterGesamtbetrag<zuZahlenderBetrag) {
		System.out.print("Noch zu zahlen: ");
		System.out.printf("%.2f", zuZahlenderBetrag - eingezahlterGesamtbetrag);
		System.out.print("Euro\n");
		System.out.print("Eingabe (mind. 50 Ct, höchstens 2 Euro): ");
		eingeworfeneMünze = tastatur.nextDouble();
		eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag; 
	}

	//Methode fahrkartenAusgeben (keine Rückgabe):
	public static void fahrkartenAusgeben(){
	
		System.out.println("\nFahrscheinausgabe in Bearbeitung...");
		for (int i = 0; i < 8; i++) {
		System.out.print("=");
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	System.out.println("\n\n");
}
	
	//Methode rueckgeldAusgeben:
	public static void rueckgeldAusgeben(double rückgabebetrag){

	if (rückgabebetrag > 0.0) {
		System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
		System.out.println("wird in folgenden Münzen ausgezahlt:");
	while (rückgabebetrag >= 2.0) // 2 €-Münzen
	{
		System.out.println("2 EURO");
		rückgabebetrag -= 2.0;
	}
	while (rückgabebetrag >= 1.0) // 1 €-Münzen
	{
		System.out.println("1 EURO");
		rückgabebetrag -= 1.0;
	}
	while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
	{
		System.out.println("50 CENT");
		rückgabebetrag -= 0.5;
	}
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
		}
	}
}
