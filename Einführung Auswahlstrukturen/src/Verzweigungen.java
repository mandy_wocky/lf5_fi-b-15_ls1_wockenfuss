<<<<<<< HEAD
import java.util.Scanner;

public class Verzweigungen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Aufgabe 1: Eigene Bedingungen
		//Erstellen Sie folgende Programme. Es werden jeweils 2 Zahlen eingegeben:		
		//... 1. Nennen Sie Wenn-Dann-Aktivit�ten aus ihrem Alltag.
		
		System.out.println("Wenn vier Stunden vergangen sind, dann habe ich noch keinen gro�en Hunger!");
		System.out.println("Wenn mehr als vier Stunden vergangen sind, habe ich gro�en Hunger!\n");

		System.out.println("Wenn ich weniger als sechs Stunden geschlafen habe, bin ich am Tag m�de.");
		System.out.println("Wenn ich mindestens sechs Stunden geschlafen habe, bin ich am Tag topfit!\n");
		
		//... 2. Wenn beide Zahlen gleich sind, soll eine Meldung ausgegeben werden (If)
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Bitte initialisieren Sie x: ");
		int x = scan.nextInt();
		System.out.println("Bitte initialisieren Sie y: ");
		int y = scan.nextInt();
		
		if (x == y) {
			System.out.println("Beide Zahlen sind gleich!\n");
		}
		
		//... 3. Wenn die 2. Zahl gr��er als die 1. Zahl ist, soll eine Meldung ausgegeben werden (If)
		
		if (x < y) {
			System.out.println("Die zweite Zahl ist gr��er als die erste Zahl.\n");
		}
			
		//... 4. Wenn die 1. Zahl gr��er oder gleich als die 2. Zahl ist, soll eine Meldung ausgegeben 
		//werden, ansonsten eine andere Meldung (If-Else)
		
		if (x >= y) {
			System.out.println("Die erste Zahl ist mindestens so gro� wie die zweite Zahl.\n");
		} else {
			System.out.println("Fehler!\n");
		}
		
		
		//Erstellen Sie folgende Programme. Es werden jeweils 3 Zahlen eingegeben:
		//1. Wenn die 1.Zahl gr��er als die 2.Zahl und die 3. Zahl ist, soll eine Meldung 
		//ausgegeben werden (If mit && / Und)
		
		System.out.println("Bitte geben Sie einen Wert f�r a an: ");
		int a = scan.nextInt();
		System.out.println("Bitte geben Sie einen Wert f�r b an: ");
		int b = scan.nextInt();
		System.out.println("Bitte geben Sie einen Wert f�r c an: ");
		int c = scan.nextInt();

		if (a > b && a > c) {
			System.out.println("Varible a ist gr��er als die Variablen b und c.");
		}
		
		//2. Wenn die 3.Zahl gr��er als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung 
		//ausgegeben werden (If mit || / Oder)
		
		if (c > b || c > a) {
			System.out.println("Variable c ist gr��er als die Variablen b oder a.");
		}
		
		//3. Geben Sie die gr��te der 3 Zahlen aus. (If-Else mit && / Und)
		
		if (a > b && a > c) {
			System.out.println("Variable a ist die gr��te Zahl.\n");
		} else if (b > a && b > c){
			System.out.println("Variable b ist die gr��te Zahl.\n");
		} else {
			System.out.println("Variable c ist die gr��te Zahl.\n");
		}

		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

		//Aufgabe 2: Steuersatz 
		//Nach der Eingabe des Nettowertes soll abgefragt werden, ob der erm��igte oder der volle 
		//Steuersatz angewendet werden soll. Der Anwender entscheidet sich �ber die Eingabe von �j�
		//f�r den erm��igten Steuersatz und mit Eingabe von �n� f�r den vollen Steuersatz. 
		//Anschlie�end soll das Programm den korrekten Bruttobetrag auf dem Bildschirm ausgeben.

		double ergebnis;
		
		Scanner steuerscanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie den Nettobetrag an:");
		int nettowert = steuerscanner.nextInt();
		System.out.println("Soll der Nettobetrag beibehalten werden?");
		System.out.println("Dr�cken Sie j f�r den erm��igten Steuersatz.");
		System.out.println("Dr�cken Sie n f�r den vollen Steuersatz.");
		char steuersatz = steuerscanner.next().charAt(0);
		
		if(steuersatz == 'j') {
			System.out.println("Der Nettobetrag ist: " + nettowert);
		}else if(steuersatz == 'n') {
			ergebnis = nettowert + (nettowert * 0.19);
			System.out.println("Der Bruttobetrag ist: " + ergebnis);
		}
		
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		
		//Aufgabe 3: Hardware-Gro�h�ndler 
		//Ein Hardware-Gro�h�ndler m�chte den Rechnungsbetrag einer Bestellung f�r PC-M�use mit 
		//einem Programm ermitteln. Wenn der Kunde mindestens 10 M�use bestellt, erfolgt die 
		//Lieferung frei Haus, bei einer geringeren Bestellmenge wird eine feste Lieferpauschale von 
		//10 Euro erhoben. Vom Gro�h�ndler werden die Anzahl der bestellten M�use und der 
		//Einzelpreis eingegeben. Das Programm soll den Rechnungsbetrag (incl. MwSt.) ausgeben.
		
		int lieferpauschale = 10;
		double ergebnis1;
		
		Scanner mausscanner = new Scanner(System.in);
		System.out.println("Anzahl der M�use:");
		int anzahlMaus = mausscanner.nextInt();
		System.out.println("Einzelpreis:");
		int nettoEinzelpreisMaus = mausscanner.nextInt();
		
		if (anzahlMaus >= 10) {
			ergebnis1 = (anzahlMaus * nettoEinzelpreisMaus) +  (anzahlMaus * nettoEinzelpreisMaus) * 0.19;
			System.out.println("Der Bruttopreis f�r " + anzahlMaus + " PC-M�use betr�gt " + ergebnis1 + " �. Sie sparen 10 � Lieferpauschalte.");
		} else if (anzahlMaus < 10) {
			ergebnis1 = (anzahlMaus * nettoEinzelpreisMaus) + (anzahlMaus * nettoEinzelpreisMaus) * 0.19 + lieferpauschale;
			System.out.println("Der Bruttopreis f�r " + anzahlMaus + " PC-M�use betr�gt zusammen mit der Lieferpauschale " + ergebnis1 + " �.");
		} else {
			System.out.println("Fehlerhafte Eingabe.");
		}
		
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		
		//Aufgabe 4: Rabattsystem 
		//Der Hardware-Gro�h�ndler f�hrt ein Rabattsystem ein: Liegt der Bestellwert zwischen 0 und 
		//100 �, erh�lt der Kunde einen Rabatt von 10 %. Liegt der Bestellwert h�her, aber insgesamt 
		//nicht �ber 500 �, betr�gt der Rabatt 15 %, in allen anderen F�llen betr�gt der Rabatt 20 %. 
		//Nach Eingabe des Bestellwertes soll der erm��igte Bestellwert (incl. MwSt.) berechnet und 
		//ausgegeben werden.
		
		double ergebnis2;
		
		Scanner bestellscanner = new Scanner(System.in);
		System.out.println("Geben Sie den Bestellwert ein:");
		int bestellwert = bestellscanner.nextInt();
		
		if (bestellwert > 0 && bestellwert < 100) {
			ergebnis2 = bestellwert - (bestellwert * 0.10);
			ergebnis2 = ergebnis2 + (ergebnis2 * 0.19);
			System.out.println("Sie erhalten einen Rabatt von 10% und zahlen damit brutto " + ergebnis2 + " �.");
		} else if (bestellwert >= 100 && bestellwert < 500) {
			ergebnis2 = bestellwert - (bestellwert * 0.15);
			ergebnis2 = ergebnis2 + (ergebnis2 * 0.19);
			System.out.println("Sie erhalten einen Rabatt von 15% und zahlen damit brutto " + ergebnis2 + " �.");
		} else if (bestellwert >= 500) {
			ergebnis2 = bestellwert - (bestellwert * 0.20);
			ergebnis2 = ergebnis2 + (ergebnis2 * 0.19);
			System.out.println("Sie erhalten einen Rabatt von 20% und zahlen damit brutto " + ergebnis2 + " �.");
		}
		
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		
		//Aufgabe 5: BMI 
		//BMI (Body Mass Index): Der BMI berechnet sich aus dem K�rpergewicht [kg] dividiert durch 
		//das Quadrat der K�rpergr��e [m2]. 
		//cDie Formel lautet: BMI = (K�rpergewicht in kg): (K�rpergr��e in m)2. 
		//Dies bedeutet, eine Person mit einer K�rpergr��e von 160 cm und einem K�rpergewicht von 
		//60 kg hat einen BMI von 23,4 [60: 2,56 = 23,4]. Der BMI einer Person wird nach folgenden 
		//Regeln klassifiziert: 
		//
		//Klassifikation   m      w
		//Untergewicht    <20    <19
		//Normalgewicht  20-25  19-24
		//�bergewicht     >25    >24
		//
		//Das Programm soll vom Benutzer das Gewicht [in kg] die Gr��e [in cm] und das Geschlecht 
		//[m/w] abfragen. Am Ende des Programms soll die BMI-Klassifikation der Person ausgegeben 
		//werden.

		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		
		//Aufgabe 6: Funktionsl�ser
		//Eine Funktion y = f(x) ist in folgenden Bereichen definiert:
		//
		//Man lese einen Wert f�r x ein und gebe den Funktionswert zusammen mit der Meldung aus, 
		//in welchem Bereich sich der Wert befindet (konstant, linear, ...). F�r die Eulersche Zahl e 
		//verwenden sie eine Konstante mit dem Wert 2,718.
=======

public class AB09_Strukturiertes_Programmieren_Verzweigungen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Aufgabe 1: Eigene Bedingungen
		//Erstellen Sie folgende Programme. Es werden jeweils 2 Zahlen eingegeben:
		
		//1. Nennen Sie Wenn-Dann-Aktivit�ten aus ihrem Alltag.
		
		
		//2. Wenn beide Zahlen gleich sind, soll eine Meldung ausgegeben werden (If)
		
		
		//3. Wenn die 2. Zahl gr��er als die 1. Zahl ist, soll eine Meldung ausgegeben werden (If)
		
		
		//4. Wenn die 1. Zahl gr��er oder gleich als die 2. Zahl ist, soll eine Meldung ausgegeben
		//werden, ansonsten eine andere Meldung (If-Else)
		
		
		
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		
		//Erstellen Sie folgende Programme. Es werden jeweils 3 Zahlen eingegeben:
		
		//1. Wenn die 1.Zahl gr��er als die 2.Zahl und die 3. Zahl ist, soll eine Meldung
		//ausgegeben werden (If mit && / Und)
		
		
		//2. Wenn die 3.Zahl gr��er als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung
		//ausgegeben werden (If mit || / Oder)
		
		//3. Geben Sie die gr��te der 3 Zahlen aus. (If-Else mit && / Und)
>>>>>>> branch 'master' of https://mandy_wocky@bitbucket.org/mandy_wocky/lf5_fi-b-15_ls1_wockenfuss.git
	}

}
