import java.util.Scanner;

public class Fallunterscheidungen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Aufgabe 1: Note
		//Erstellen Sie die Konsolenanwendung Noten. Das Programm Noten soll nach der Eingabe
		//einer Ziffer die sprachliche Umschreibung ausgeben (1 = Sehr gut, 2 = Gut, 3 = Befriedigend,
		//4 = Ausreichend, 5 = Mangelhaft, 6 = Ungen�gend). Falls eine andere Ziffer eingegeben
		//wird, soll ein entsprechender Fehlerhinweis ausgegeben werden. 
		
		int note;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte gebe eine Note ein:");
		note = myScanner.nextInt();
	
		
		switch(note) {
		case 1:
			System.out.println("Sehr gut");
			break;
		case 2:
			System.out.println("Gut");
			break;
		case 3:
			System.out.println("Befriedigend");
			break;
		case 4:
			System.out.println("Ausreihend");
			break;
		case 5:
			System.out.println("Mangelhaft");
			break;
		case 6:
			System.out.println("Ungen�gend");
			break;
		default:
<<<<<<< HEAD
			System.out.println("Bitte gib eine Note von 1 bis 6 ein.");
=======
			System.out.println("Fehler!");
			break;
		}		
		
		//Aufgabe 2: Monate
		//Der Benutzer gibt eine Ziffer zwischen 1 und 12 ein. Ihr Java-Programm ermittelt den
		//entsprechenden Monat.
		
		int month;
		
		Scanner monthScanner = new Scanner (System.in);
		System.out.println("\nBitte gebe eine Ziffer zwischen 1 und 12 ein: ");
		month = monthScanner.nextInt();
		
		switch (month) {
		case 1:
			System.out.println("Januar");
			break;
		case 2:
			System.out.println("Februar");
			break;
		case 3:
			System.out.println("M�rz");
			break;
		case 4:
			System.out.println("April");
			break;
		case 5:
			System.out.println("Mai");
			break;
		case 6:
			System.out.println("Juni");
			break;
		case 7:
			System.out.println("Juli");
			break;
		case 8:
			System.out.println("August");
			break;
		case 9:
			System.out.println("September");
			break;
		case 10:
			System.out.println("Oktober");
			break;
		case 11:
			System.out.println("November");
			break;
		case 12:
			System.out.println("Dezember");
			break;
		default:
			System.out.println("Fehler!");	
>>>>>>> branch 'master' of https://mandy_wocky@bitbucket.org/mandy_wocky/lf5_fi-b-15_ls1_wockenfuss.git
			break;
		}
		
		//Aufgabe 3: R�mische Zahlen
		//Erstellen Sie die Konsolenanwendung Rom. Das Programm Rom soll nach der Eingabe 
		//eines r�mischen Zahlzeichens die entsprechende Dezimalzahl ausgeben (I = 1, V = 5, X = 
		//10, L = 50, C = 100, D = 500, M = 1000). Falls ein anderes Zeichen eingegeben wird, soll ein 
		//entsprechender Hinweis ausgegeben werden. 
		
		Scanner romScanner = new Scanner(System.in);
		System.out.println("\nBitte gebe ein r�misches Zahlenzeichen ein (I, V, X, L, C, D oder M):");
		char rom = romScanner.next().charAt(0);
		
		switch(rom) {
		case 'I':
			System.out.println("I = 1");
			break;
		case 'V':
			System.out.println("V = 5");
			break;
		case 'X':
			System.out.println("X = 10");
			break;
		case 'L':
			System.out.println("L = 50");
			break;
		case 'C':
			System.out.println("C = 100");
			break;
		case 'D':
			System.out.println("D = 500");
		case 'M':
			System.out.println("M = 1000");
			break;
		default:
			System.out.println("Fehler!");
		}
		
		//Aufgabe 4: Taschenrechner 
		//Der Benutzer soll zwei Zahlen in das Programm eingeben, danach soll er entscheiden, ob die 
		//Zahlen addiert, subtrahiert, multipliziert oder dividiert werden, diese Entscheidung soll �ber die 
		//Eingabe der folgenden Symbole get�tigt werden: +, -, *, /
		//Nach der Auswahl soll das Ergebnis der Rechnung ausgegeben werden, bzw. eine 
		//Fehlermeldung, falls eine falsche Auswahl getroffen wurde
		
		double num1, num2, result;
		char operator;
	
		Scanner scannervariable = new Scanner(System.in);
		System.out.println("Bitte gib eine Zahl ein: ");
		num1 = scannervariable.nextDouble();
		
		System.out.println("Bitte gib eine zweite Zahl ein: ");
		num2 = scannervariable.nextDouble();
		
		System.out.println("Bitte w�hle eine Rechenoperation (+, -, *, /): ");
		operator = scannervariable.next().charAt(0);
		
		switch(operator) {
		case '+':
			result = num1 + num2;
			System.out.println("Das Ergebnis ist: " + result);
			break;
		case '-':
			result = num1 - num2;
			System.out.println("Das Ergebnis ist: " + result);
			break;
		case '*':
			result = num1 * num2;
			System.out.println("Das Ergebnis ist: " + result);
			break;
		case '/':
			result = num1 / num2;
			System.out.println("Das Ergebnis ist: " + result);
			break;
		default:
			System.out.println("Fehler!");
		}
		
		//Aufgabe 5: Ohmsches Gesetz
		//Nach dem Ohmschen Gesetz berechnet sich der Widerstand eines ohmschen Widerstandes mit:
		//R = U/I
		//Schreiben Sie ein Programm, in das der Benutzer zun�chst �ber die Eingabe der 
		//Buchstaben R, U oder I ausw�hlen kann, welche Gr��e berechnet werden soll. Gibt er einen 
		//falschen Buchstaben ein, soll eine Meldung �ber die Fehleingabe erfolgen.
		//Anschlie�end soll er die Werte der fehlenden Gr��en eingeben. Am Ende gibt das 
		//Programm den Wert der gesuchten Gr��e mit der richtigen Einheit aus.
		//"U" ist die Spannung;
		//"R" ist der Widerstand;
		//"I" ist der Strom;
		
		char ohmschesGesetz;
		double widerstand, spannung, strom, ergebnis;
		
		Scanner ohmscanner = new Scanner(System.in);
		System.out.println("Welche Variable m�chtest du berechnen (U, R oder I): ");
		ohmschesGesetz = ohmscanner.next().charAt(0);
		
		switch(ohmschesGesetz) {
		case 'U':
			System.out.println("Du m�chtest die Spannung berechnen.\n");
			System.out.println("Bitte gebe den Wert f�r den Widerstand R an:\n");
			widerstand = ohmscanner.nextDouble();
			System.out.println("Bitte gebe den Wert f�r den Strom I an:\n");
			strom = ohmscanner.nextDouble();
			ergebnis = widerstand/strom;
			System.out.println("Die Spannung betr�gt: " + ergebnis);
			break;
		case 'R':
			System.out.println("Du m�chtest den Widerstand berechnen.\n");
			System.out.println("Bitte gebe den Wert f�r die Spannung U an:\n");
			spannung = ohmscanner.nextDouble();
			System.out.println("Bitte gebe den Wert f�r den Strom I an:\n");
			strom = ohmscanner.nextDouble();
			ergebnis = spannung/strom;
			System.out.println("Der Widerstand betr�gt: " + ergebnis);
			break;
		case 'I':
			System.out.println("Du m�chtest den Strom berechnen.\n");
			System.out.println("Bitte gebe den Wert f�r die Spannung U an: ");
			spannung = ohmscanner.nextDouble();
			System.out.println("Bitte gebe den Wert f�r den Widerstand R an: ");
			widerstand = ohmscanner.nextDouble();
			ergebnis = spannung * widerstand;
			System.out.println("Der Strom betr�gt: " + ergebnis);
			break;
	r	default:
			System.out.println("Fehlerhafte Eingabe!");
		}
	}
}
