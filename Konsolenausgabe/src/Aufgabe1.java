
public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Aufgabe 1
		
		int alter = 34;
		String name = "Mandy";
	
		System.out.print("Mein Name ist \"" + name + "\".\n");
		System.out.println("Ich bin " + alter + " Jahre alt.\n\n");
		
		// Bei der print()-Anweisung erscheint die Ausgabe ohne Zeilenvorschub.
		// Bei der println()-Anweisung erscheint die Ausgabe ohne Zeilenvorschub.
		// Aufgabe 2
		
		System.out.println("      *      ");
		System.out.println("     ***     ");
		System.out.println("    *****    ");
		System.out.println("   *******   ");
		System.out.println("  *********  ");
		System.out.println("     ***     ");
		System.out.println("     ***     \n\n");
		
		// Aufgabe 3
		
		double a = 22.4234234;
		System.out.printf("%.2f\n", a);
		
		double b = 111.2222;
		System.out.printf("%.2f\n", b);
		
		double c = 4.0;
		System.out.printf("%.2f\n", c);
		
		double d = 1000000.551;
		System.out.printf("%.2f\n", d);
		
		double e = 97.34;
		System.out.printf("%.2f\n", e);
	}

}
